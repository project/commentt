<?php

/**
 * Contains the 'timediff' field handler.
 */
class commentt_handler_field_timediff extends views_handler_field {
  
  function query() {
    $this->field_alias = 'commentt_timediff_'. $this->position;
  }
  
  function render($values) {
    return format_interval($values->commentt_stop - $values->commentt_start);
  }
}
