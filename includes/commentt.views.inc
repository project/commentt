<?php

/**
 *
 */
function commentt_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'commentt') . '/includes',
    ),
    'handlers' => array(
      'commentt_handler_field_timediff' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 *
 */
function commentt_views_data() {

  $data['commentt']['table']['group'] = t('commentt');

  $data['commentt']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
    'type' => 'INNER'
  );
  $data['commentt']['table']['join']['users'] = array(
    'left_field' => 'uid',
    'field' => 'uid',
    'type' => 'INNER'
  );
  $data['commentt']['table']['join']['comments'] = array(
    'left_field' => 'cid',
    'field' => 'cid',
    'type' => 'INNER'
  );

  // commentt ID field
  $data['commentt']['yid'] = array(
    'title' => t('commentt Id'),
    'help' => t('The commentt Id'),
    'field' => array(
        'handler' => 'views_handler_field',
    ),
  );

  // commentt nid
  $data['commentt']['nid'] = array(
    'title' => t('Service ID'),
    'help' => t('The Service ID'),
    'relationship' => array(
        'base' => 'node',
        'field' => 'nid',
        'handler' => 'views_handler_relationship',
        'label' => t('Service Id'),
    ),
  );

  // commentt uid
  $data['commentt']['uid'] = array(
    'title' => t('User'),
    'help' => t('The User'),
    'relationship' => array(
        'base' => 'users',
        'field' => 'uid',
        'handler' => 'views_handler_relationship',
        'label' => t('User'),
    ),
  );

  // commentt cid
  $data['commentt']['cid'] = array(
    'title' => t('Comment ID'),
    'help' => t('The Comment ID'),
    'relationship' => array(
        'base' => 'comment',
        'field' => 'cid',
        'handler' => 'views_handler_relationship',
        'label' => t('Comment Id'),
    ),
  );

  // commentt note
  $data['commentt']['note'] = array(
    'title' => t('Note'),
    'help' => t('a note'),
    'field' => array(
        'handler' => 'views_handler_field',
        'click_sortable' => FALSE,
    ),
  );

  // commentt start timestamp
  $data['commentt']['start'] = array(
    'title' => t('Start'),
    'help' => t('Start time'),
    'field' => array(
        'handler' => 'views_handler_field_date',
    ),
  );

  // commentt stop timestamp
  $data['commentt']['stop'] = array(
    'title' => t('Stop'),
    'help' => t('Stop time'),
    'field' => array(
        'handler' => 'views_handler_field_date',
    ),
    'filter' => array(
        'handler' => 'views_handler_filter_numeric'
    ),
  );

  // commentt created timestamp
  $data['commentt']['created'] = array(
    'title' => t('Created'),
    'help' => t('Created time'),
    'field' => array(
        'handler' => 'views_handler_field_date',
    ),
    'filter' => array(
        'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
        'handler' => 'views_handler_sort_date',
    ),
  );

  $data['commentt']['timediff'] = array(
    'title' => t('Time'),
    'help' => t('Time / Duration'),
    'field' => array(
        'handler' => 'commentt_handler_field_timediff',
        'notafield' => TRUE,
    ),
  );

  return $data;
}
